module Plugin::SupportTickets::UI
  include BusinessR::UI

  define :ui do
    main_menu_item "Customer" do
      menu_item "Support Tickets" do
        on_click :open_view => "List"
        menu_item "List all" { on_click :open_view => "List" }
        menu_item "Search"   { on_click :open_view => "Search" }
        menu_item "New"      { on_click :open_view => "New" }
      end
    end
  end

  class New < BaseUI::New
  end

  class List < BaseUI::List
    define :data_source do
    end

    refine :ui do
      within :main_list do
        within :header do
          add_button :mark_all_as_read
        end

        change :background_color, :to => :black
      end
    end
  end

  class Detail < BaseUI::Detail
  end
end